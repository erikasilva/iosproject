//
//  ClientViewProductViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/23/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class ClientViewProductViewController: UIViewController, UITextViewDelegate {

    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextView!
    
    @IBOutlet weak var codeTextField: UITextField!
    
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var addToCartButton: UIButton!
    
    var categoryId: String = ""
    var productId: String = ""
    var isFavourite: Bool = false
    var isFromCart: Bool = false
    var currentUser = Auth.auth().currentUser
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setButton(imageName: "whiteHeart.png")
        
        getIsFavouriteProduct()
        getIsProductinCart()
        
        productImageView.image = #imageLiteral(resourceName: "category")
        
        descriptionTextField.delegate = self
        
        let color = UIColor(red: 65/255, green: 173/255, blue: 170/255, alpha: 1.0).cgColor
        descriptionTextField.layer.borderColor = color
        descriptionTextField.layer.borderWidth = 2.0
        descriptionTextField.layer.cornerRadius = 5
        
        if(isFromCart == true){
            addToCartButton.setTitle("Remove from Cart", for: .normal)
        }
        
        
        setToViewInformation()

    }
    
 
    
    
    func setButton(imageName: String){
        let buttonFavourites: UIButton = UIButton(type: UIButton.ButtonType.custom)
        buttonFavourites.setImage(UIImage(named: imageName), for: UIControl.State.normal)
        buttonFavourites.addTarget(self, action: #selector(favouriteButtonTapped), for: UIControl.Event.touchUpInside)
        
        let barButton = UIBarButtonItem(customView: buttonFavourites)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    
    @objc func favouriteButtonTapped(){
        if !self.isFavourite{
            self.showAlertErrorAddFavouritesButton(tittle: "Add to Favourites", message: "Are you sure to add item to Favourites?")
        }else{
            self.showAlertErrorRemoveFavouritesButton(tittle: "Remove from Favourites", message: "Are you sure to remove item from Favourites?")
        }
    }
    
    
    func getIsFavouriteProduct(){
        db.collection("Users").document(currentUser!.uid).collection("FavouritesProducts")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        if(document.get("productId") as? String == self.productId){
                            self.setButton(imageName: "blackHeart.png")
                            self.isFavourite = true
                        }
                    }
                }
        }
    }
    
    
    func getIsProductinCart(){
        db.collection("Users").document(currentUser!.uid).collection("CartProducts")
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        if(document.get("productId") as? String == self.productId){
                            self.isFromCart = true
                            self.addToCartButton.setTitle("Remove from Cart", for: .normal)
                        }
                    }
                }
        }
    }
    
    
    func addToFavourites(){
        db.collection("Users").document(currentUser!.uid).collection("FavouritesProducts").addDocument(data: [
            "productId": self.productId,
            "categoryId": self.categoryId
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                self.showAlertError(tittle: "Error", message: "Error adding item in Favourites")
            }
        }
        
    }
    
    
    func removeFromFavourites(){
        let favouritesRef = db.collection("Users").document(currentUser!.uid).collection("FavouritesProducts")
        favouritesRef.getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        if(document.get("productId") as? String == self.productId){
                            favouritesRef.document(document.documentID).delete()
                        }
                    }
                }
        }
    }
    
    
  
    
    func setToViewInformation(){
        productImageView.isUserInteractionEnabled = false
        nameTextField.isUserInteractionEnabled = false
        nameTextField.textColor = UIColor.lightGray
        descriptionTextField.isUserInteractionEnabled = false
        descriptionTextField.textColor = UIColor.lightGray
        codeTextField.isUserInteractionEnabled = false
        codeTextField.textColor = UIColor.lightGray
        priceTextField.isUserInteractionEnabled = false
        priceTextField.textColor = UIColor.lightGray
        
        setProductInformation()
    }
    
    func setProductInformation(){
        let productRef = db.collection("Categories").document(categoryId).collection("Products").document(productId)
        
        productRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.nameTextField.text = document.get("name") as? String
                self.descriptionTextField.text = document.get("description") as? String
                self.codeTextField.text = document.get("code") as? String
                self.priceTextField.text = "\(document.get("price") as! Double)"
                self.productImageView.kf.setImage(with: URL(string: document.get("imageUrl") as! String))
                
            } else {
                print("Document does not exist")
            }
        }
        
    }
    
    @IBAction func addToCartButtonPressed(_ sender: Any) {
        if (isFromCart == false){
            showAlertErrorAddToCart(tittle: "Add to Cart", message: "Are you sure to add item to cart?")
        }else{
            showAlertErrorRemoveFromCart(tittle: "Remove from Cart", message: "Are you sure to remove from cart?")
        }
    }
    
    
    func addToCart(){
        db.collection("Users").document(currentUser!.uid).collection("CartProducts").addDocument(data: [
            "productId": self.productId,
            "categoryId": self.categoryId
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
                self.showAlertError(tittle: "Error", message: "Error adding item in Cart")
            }
        }
    }
    
    
    func removeFromCart(){
        let cartRef = db.collection("Users").document(currentUser!.uid).collection("CartProducts")
        cartRef.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if(document.get("productId") as? String == self.productId){
                        cartRef.document(document.documentID).delete()
                    }
                }
            }
        }
    }
    
    
    
    func showAlertErrorAddFavouritesButton(tittle:String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            self.setButton(imageName: "blackHeart.png")
            self.addToFavourites()
            _ = self.navigationController?.popViewController(animated: true)
        }
        let cancelAlterAction = UIAlertAction(title: "Cancel", style: .default) { [unowned self] (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
        alertView.addAction(cancelAlterAction)
    }
    
    
    func showAlertErrorRemoveFavouritesButton(tittle:String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            self.setButton(imageName: "whiteHeart.png")
            self.removeFromFavourites()
            _ = self.navigationController?.popViewController(animated: true)
        }
        let cancelAlterAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
        alertView.addAction(cancelAlterAction)
    }
    
    
    func showAlertErrorAddToCart(tittle:String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            self.addToCart()
            _ = self.navigationController?.popViewController(animated: true)
        }
        let cancelAlterAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
        alertView.addAction(cancelAlterAction)
    }
    
    
    func showAlertErrorRemoveFromCart(tittle:String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.removeFromCart()
            _ = self.navigationController?.popViewController(animated: true)
        }
        let cancelAlterAction = UIAlertAction(title: "Cancel", style: .default) { (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
        alertView.addAction(cancelAlterAction)
    }
    
    
    func showAlertError(tittle:String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
    }


}
