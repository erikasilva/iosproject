//
//  CategoriesViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/16/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Kingfisher


class CategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    @IBOutlet weak var categories_tableView: UITableView!
    
    let db = Firestore.firestore()
    var categories = [Category]()
    var categoryId: String? = ""
    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()

    }
    
    func getCategories(){
        db.collection("Categories").addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            self.categories = []
            for document in documents {
                let category = Category(
                    document.documentID,
                    document.get("name") as! String,
                    document.get("description") as! String,
                    document.get("imageUrl") as! String
                )
                self.categories.append(category)
            }
            self.categories_tableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories.count
    }

    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! CategoryTableViewCell
        cell.categoryName_label.text = categories[indexPath.row].name
        cell.categoryDescription_label.text = categories[indexPath.row].description
        cell.category_imageView.kf.setImage(with: URL(string: categories[indexPath.row].imageUrl))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.categoryId = categories[indexPath.row].id
        performSegue(withIdentifier: "toViewProducts", sender: self)
    }
    
    
    
    
    @IBAction func addCategory_button(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "toAddCategory", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toViewProducts" {
            let destination = segue.destination as! ProductsViewController
            
            let categoryIdParameter = self.categoryId
            destination.categoryId = categoryIdParameter!
        }
        
        if segue.identifier == "toAddCategory" {
            let destination = segue.destination as! AddNewCategoryViewController
            destination.categoryId = self.categoryId!
            destination.isForViewCategory = false
        }
        
    }
    
}
