//
//  Category.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/16/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import Foundation


class Category{
    var id: String
    var name: String
    var description:String
    var imageUrl:String
    
    
    init(_ id: String, _ name: String, _ description: String, _ imageUrl:String) {
        self.id = id
        self.name = name
        self.description = description
        self.imageUrl = imageUrl
    }
    
}
