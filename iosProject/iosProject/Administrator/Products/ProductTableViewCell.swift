//
//  ProductTableViewCell.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/19/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var productNameTextView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
