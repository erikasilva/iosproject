//
//  RecoverPasswordViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/29/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseAuth

class RecoverPasswordViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func recoverPasswordButtonPressed(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: (emailTextField.text)!) { error in
            self.showAlertErrorOk(tittle: "Email Send Succesfully", message: "Email send to \(self.emailTextField.text as? String), check your email!")
        }
        
    }
    
    func showAlertErrorOk(tittle:String, message:String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            self.dismiss(animated: true, completion: nil)
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
    }
    
    func showAlertError(tittle:String, message:String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
    }
    
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
