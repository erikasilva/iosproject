//
//  ClientProductsViewController.swift
//  iosProject
//
//  Created by Erika Silva on 7/22/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore

class ClientProductsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var productsTableView: UITableView!
    
    var categoryId: String = ""
    var productId: String = ""
    let db = Firestore.firestore()
    var products = [Product]()
    var isForViewProduct: Bool = true
    
    var searchController: UISearchController!
    var filteredProduct = [Product]()
    var isFiltering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProducts()
        configSearchbar()
    }
    
    
    
    
    func getProducts(){
        db.collection("Categories").document(categoryId).collection("Products").addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            self.products = []
            for document in documents {
                let product = Product(
                    document.documentID,
                    document.get("name") as! String,
                    document.get("description") as! String,
                    document.get("code") as! String,
                    document.get("price") as! Double,
                    document.get("imageUrl") as! String
                )
                self.products.append(product)
            }
            self.productsTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredProduct.count : products.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! ProductTableViewCell
        productId = products[indexPath.row].id
        
        var currentProduct: Product!
        if(isFiltering){
            currentProduct = filteredProduct[indexPath.row]
        }else{
            currentProduct = products[indexPath.row]
        }
        
        cell.productNameTextView.text = currentProduct.name
        cell.productImageView.kf.setImage(with: URL(string: currentProduct.imageUrl))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.productId = products[indexPath.row].id
        performSegue(withIdentifier: "toClientViewProduct", sender: self)
    }
    
    func configSearchbar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        productsTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredProduct = products.filter{
            ($0.name)
                .lowercased()
                .contains((searchController.searchBar.text ?? "").lowercased()
            )}
        isFiltering = searchController.searchBar.text != ""
        productsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toClientViewProduct" {
            let destination = segue.destination as! ClientViewProductViewController
            destination.categoryId = categoryId
            destination.productId = self.productId
        }
    }
    
    
}
