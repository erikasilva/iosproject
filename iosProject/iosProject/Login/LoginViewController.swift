//
//  LoginViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 6/25/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var createAccountButton: UIButton!
    
    
    var db: Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        self.buttonCustomization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //ve si tiene un usuario logueado, si no hay hace return
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        self.getUserType(currentUser.uid)
        
        
    }
    
    func buttonCustomization(){
        self.logInButton.layer.cornerRadius = 5
        self.logInButton.layer.borderWidth = 1
        
        self.createAccountButton.layer.cornerRadius = 5
        self.createAccountButton.layer.borderWidth = 1
    }
    
    
    @IBAction func forgotPasswordButton(_ sender: Any) {
        self.performSegue(withIdentifier: "toRecoverPassword", sender: self)
    }
    
    func performSegueToAdminUser(){
        self.performSegue(withIdentifier: "toAdminTabBarController", sender: self)
    }
    
    func performSegueToClientUser(){
        self.performSegue(withIdentifier: "toClientTabBarController", sender: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        firebaseAuth(email: emailTextField.text!, password: passwordTextField.text!)
    }
    
    
    @IBAction func createAccountButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toCreateAccountSegue", sender: self)
    }
    
    
    
    func firebaseAuth(email: String, password: String){
        Auth.auth().signIn(withEmail: email, password: password){(result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.getUserType(result?.user.uid)
        }
    }
    
    func getUserType(_ currentUserID: String?){
        let userRef = db.collection("Users").document(currentUserID!)
        userRef.getDocument { (user, error) in
            if let user = user, user.exists {
                let userType = user.get("type")! as? String ?? ""
                if userType == "Administrator"{
                    self.performSegueToAdminUser()
                }else{
                    self.performSegueToClientUser()
                }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials", preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
          
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
    }
    
    
}
