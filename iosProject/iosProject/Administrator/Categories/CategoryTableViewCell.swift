//
//  CategoryTableViewCell.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/16/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var categoryName_label: UILabel!
 
    @IBOutlet weak var category_imageView: UIImageView!
    
    @IBOutlet weak var categoryDescription_label: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
