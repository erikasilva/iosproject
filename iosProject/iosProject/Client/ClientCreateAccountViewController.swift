//
//  ClientCreateAccountViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/26/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

extension Date{
    static let formatterClient: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }()
    var formattedClient: String{
        return Date.formatter.string(from: self)
    }
}


class ClientCreateAccountViewController: UIViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var userAccountImageView: UIImageView!
    
    @IBOutlet weak var addImageButton: UIButton!
    
    @IBOutlet weak var fullNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var birthdateTextField: UITextField!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    
    
    
    let db = Firestore.firestore()
    let storage = Storage.storage()
    
    let datePickerView = UIDatePicker()
    let imagePickerController = UIImagePickerController()
    var buttonEdit: UIBarButtonItem?
    var isCancelPressed = false
    
    override func viewWillAppear(_ animated: Bool) {
        birthdateTextField.inputView = datePickerView
        
        userAccountImageView.layer.cornerRadius = userAccountImageView.frame.height / 2.0
        userAccountImageView.layer.masksToBounds = true
        
        buttonCustomization()
    }
    
    func buttonCustomization(){
        self.logOutButton.layer.cornerRadius = 5
        self.logOutButton.layer.borderWidth = 1
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserInfo()
        
        userAccountImageView.image = #imageLiteral(resourceName: "accountIcon")
        
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        self.navigationItem.rightBarButtonItem = self.editButtonItem
        imagePickerController.delegate = self
        
        setUserInteraction(isUserInteractionEnabled: false, color: UIColor.gray)
    }
    
    
    
    @objc func handleDatePicker(_ datePicker:UIDatePicker){
        birthdateTextField.text = datePickerView.date.formatted
    }
    
    
    
    func getUserInfo(){
        
        guard let currentUser = Auth.auth().currentUser else{
            return
        }
        let user_ref = db.collection("Users").document(currentUser.uid)
        
        user_ref.getDocument{(snapshot, error) in
            if let error = error{
                print("Error getting documents: \(error)")
            }else{
                self.fullNameTextField.text = snapshot!.data()?["fullName"] as? String
                self.emailTextField.text = snapshot!.data()?["email"] as? String
                self.birthdateTextField.text = snapshot!.data()?["birthday"] as? String
                self.userAccountImageView.kf.setImage(with: URL(string: snapshot!.data()?["imageUrl"] as? String ?? ""))
            }
        }
    }
    
    
    override func setEditing (_ editing:Bool, animated:Bool)
    {
        super.setEditing(editing,animated:animated)
        if(self.isEditing)
        {
            setUserInteraction(isUserInteractionEnabled: true, color: UIColor.black)
            self.setLeftCancelButton()
            self.isCancelPressed = false
        }else{
            setUserInteraction(isUserInteractionEnabled: false, color: UIColor.gray)
            if (!isCancelPressed){
                saveUserInformation()
                saveUserProfileImage(userId: Auth.auth().currentUser?.uid ?? "")
            }
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    
    
    func setLeftCancelButton(){
        buttonEdit = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(setUserInteractionObjc))
        self.navigationItem.leftBarButtonItem = buttonEdit
    }
    
    
    @objc func setUserInteractionObjc(){
        self.isCancelPressed = true
        self.isEditing = false
        self.getUserInfo()
    }
    
    func setUserInteraction(isUserInteractionEnabled:Bool, color: UIColor){
        self.addImageButton.isUserInteractionEnabled = isUserInteractionEnabled
        self.fullNameTextField.isUserInteractionEnabled = isUserInteractionEnabled
        self.fullNameTextField.textColor = color
        self.emailTextField.isUserInteractionEnabled = isUserInteractionEnabled
        self.emailTextField.textColor = color
        self.birthdateTextField.isUserInteractionEnabled = isUserInteractionEnabled
        self.birthdateTextField.textColor = color
        self.userAccountImageView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    
    
    @IBAction func addImageButton(_ sender: Any) {
        addPictureButtonPressed()
    }
    
    
    
    func addPictureButtonPressed() {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default){(_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default){(_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancel)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("error picking image")
            return
        }
        userAccountImageView.image = image
        
    }
    
    
    func saveUserInformation() {
        guard let currentUser = Auth.auth().currentUser else{
            return
        }
        let user_ref = db.collection("Users").document(currentUser.uid)
        
        user_ref.updateData([
            "birthday": self.birthdateTextField.text ?? "",
            "email": self.emailTextField.text ?? "",
            "fullName": self.fullNameTextField.text ?? "",
            "type": "Client"
        ]) { err in
            if let err = err {
                self.showAlert(title: "Something went wrong", message: "Please fill out the fields again!")
                print(err)
            } else {
                self.showAlert(title: "Update successfully", message: "Information was updated correctly!")
            }
        }
    }
    
    func saveUserProfileImage(userId:String){
        
        let usersImages = storage.reference().child("users")
        let currentUserImage = usersImages.child("\(userId).jpg")
        let userImage = userAccountImageView.image
        let data = userImage?.jpegData(compressionQuality: 1)
        if userImage != nil{
            _ = currentUserImage.putData(data!, metadata: nil) { (metadata, error) in
                currentUserImage.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        return
                    }
                    self.updateUserImage(id: userId, imageUrl: "\(downloadURL)")
                }
            }
        }
    }
    
    func updateUserImage(id: String, imageUrl: String){
        let categoryRef = db.collection("Users").document(id)
        
        categoryRef.updateData([
            "imageUrl": imageUrl
        ]) { err in
            if let err = err {
                print(err)
                self.showAlert(title: "Profile photo", message: "Something went wrong, please try again.")
                
            } else {
                print("Document successfully updated")
                //self.showAlert(title: "Profile photo", message: "Profile photo successfully updated!")
            }
        }
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            
        }
    }
    
    func showAlert(title:String, message:String){
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default)
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
        
    }



}
