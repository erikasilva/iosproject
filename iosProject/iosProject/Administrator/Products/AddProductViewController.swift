//
//  AddProductViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/19/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage

class AddProductViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var productNameTextField: UITextField!
    
    @IBOutlet weak var productDescriptionTextView: UITextView!
    
    @IBOutlet weak var productCodeTextField: UITextField!
    
    @IBOutlet weak var productPriceTextField: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var addImageButton: UIButton!
    
    
    var isForViewproduct: Bool = true
    var categoryId: String = ""
    var productId: String = ""
    
    let imagePickerController = UIImagePickerController()
    var buttonEdit: UIBarButtonItem?
    
    let db = Firestore.firestore()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if(isForViewproduct){
            self.title = "Product Information"
            setToViewInformation()
            buttonEdit = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editButtonTapped))
            self.navigationItem.rightBarButtonItem = buttonEdit
        }
        
        productImageView.image = #imageLiteral(resourceName: "category")
        
        productDescriptionTextView.delegate = self
        productDescriptionTextView.text = "Description"
        productDescriptionTextView.textColor = UIColor.lightGray
        
        let color = UIColor(red: 65/255, green: 173/255, blue: 170/255, alpha: 1.0).cgColor
        productDescriptionTextView.layer.borderColor = color
        productDescriptionTextView.layer.borderWidth = 2.0
        productDescriptionTextView.layer.cornerRadius = 5
        
        imagePickerController.delegate = self
        
        self.buttonCustomization()

    }
    
    
    func buttonCustomization(){
        saveButton.layer.cornerRadius = 5
        saveButton.layer.borderWidth = 1
    }
    
    
    @objc func cancelButtonTapped(sender: UIBarButtonItem){
        setToViewInformation()
    }
    
    func setToViewInformation(){
        
        addImageButton.isUserInteractionEnabled = false
        
        productImageView.isUserInteractionEnabled = false
        productNameTextField.isUserInteractionEnabled = false
        productNameTextField.textColor = UIColor.lightGray
        productDescriptionTextView.isUserInteractionEnabled = false
        productDescriptionTextView.textColor = UIColor.lightGray
        productCodeTextField.isUserInteractionEnabled = false
        productCodeTextField.textColor = UIColor.lightGray
        productPriceTextField.isUserInteractionEnabled = false
        productPriceTextField.textColor = UIColor.lightGray
        
        buttonEdit = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editButtonTapped))
        self.navigationItem.rightBarButtonItem = buttonEdit
        
        setProductInformation()
    }
    
    func setProductInformation(){
        let productRef = db.collection("Categories").document(categoryId).collection("Products").document(productId)
        
        productRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.productNameTextField.text = document.get("name") as? String
                self.productDescriptionTextView.text = document.get("description") as? String
                self.productCodeTextField.text = document.get("code") as? String
                self.productPriceTextField.text = "\(document.get("price") as! Double)"
                self.productImageView.kf.setImage(with: URL(string: document.get("imageUrl") as! String))
                
            } else {
                print("Document does not exist")
            }
        }
    }
    
    
    @objc func editButtonTapped(){
        setToEditView()
    }
    
    

    
    func setToEditView(){
        addImageButton.isUserInteractionEnabled = true
        
        productImageView.isUserInteractionEnabled = true
        productNameTextField.isUserInteractionEnabled = true
        productNameTextField.textColor = UIColor.black
        productDescriptionTextView.isUserInteractionEnabled = true
        productDescriptionTextView.textColor = UIColor.black
        productCodeTextField.isUserInteractionEnabled = true
        productCodeTextField.textColor = UIColor.black
        productPriceTextField.isUserInteractionEnabled = true
        productPriceTextField.textColor = UIColor.black
        
        self.buttonEdit = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelButtonTapped(sender:)))
        self.navigationItem.rightBarButtonItem = buttonEdit
    }
    
    
    
   
    @IBAction func addImageButton(_ sender: Any) {
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else {
            print("Error Picking image")
            return
        }
        productImageView.image = image
    }
    
    
    
    @IBAction func saveButton(_ sender: Any) {
        saveProductInfo()
    }
    
    
    func saveProductInfo(){
        if(self.isForViewproduct){
            let productRef = db.collection("Categories").document(categoryId).collection("Products").document(productId)
            productRef.updateData([
                "name": productNameTextField.text ?? "",
                "description": productDescriptionTextView.text ?? "",
                "code": productCodeTextField.text ?? "",
                "price": Double(productPriceTextField.text!)!,
                "imageUrl": ""
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    self.storeProductPhoto(id: productRef.documentID)
                }
            }
        }else{
            var productRef: DocumentReference? = nil
            productRef = db.collection("Categories").document(categoryId).collection("Products").addDocument(data: [
                "name": productNameTextField.text ?? "",
                "description": productDescriptionTextView.text ?? "",
                "code": productCodeTextField.text ?? "",
                "price": Double(productPriceTextField.text!)!,
                "imageUrl": ""
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID")
                    self.storeProductPhoto(id: productRef!.documentID)
                }
            }
        }
    }
    
    
    
    func storeProductPhoto(id: String){
        let storage = Storage.storage()
        let productsImages = storage.reference().child("products")
        let currentProductImage = productsImages.child("\(id).jpg")
        
        let productImage = productImageView.image
        let data = productImage?.jpegData(compressionQuality: 1)!
        
        _ = currentProductImage.putData(data!, metadata: nil) { (metadata, error) in
            currentProductImage.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.updateProductImageUrl(id: id, imageUrl: "\(downloadURL)")
            }
        }
    }
    
    
    func updateProductImageUrl(id: String, imageUrl: String){
        let productRef = db.collection("Categories").document(categoryId).collection("Products").document(id)
        
        productRef.updateData([
            "imageUrl": imageUrl
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
                self.showAlertError(tittle: "Success", message: "Product was upload Succesfully")
            }
        }
    }
    
    func showAlertError(tittle: String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "Ok", style: .default) { [unowned self] (_) in
            _ = self.navigationController?.popViewController(animated: true)
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if productDescriptionTextView.textColor == UIColor.lightGray {
            productDescriptionTextView.text = ""
            productDescriptionTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if productDescriptionTextView.text == "" {
            productDescriptionTextView.text = "Description"
            productDescriptionTextView.textColor = UIColor.lightGray
        }
    }
    
    
    
    
    
    
}
