//
//  ClientFavouritesViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/24/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class ClientFavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    
    @IBOutlet weak var productsTableView: UITableView!
    
    let db = Firestore.firestore()
    var productId: String = ""
    var categoryId: String = ""
    var products = [Product]()
    var categories = [String]()
    var currentUser = Auth.auth().currentUser
    
    var searchController: UISearchController!
    var filteredProduct = [Product]()
    var isFiltering = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFavouritesProducts()
        configSearchbar()
    }
   
    
    func getFavouritesProducts(){
        db.collection("Users").document(currentUser!.uid).collection("FavouritesProducts").addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            self.products.removeAll()
            self.categories.removeAll()
            for document in documents {
                self.categoryId = document.get("categoryId") as! String
                self.productId = document.get("productId") as! String
                self.categories.append(self.categoryId)
                self.getProduct()
            }
            self.productsTableView.reloadData()
        }
        
    }
    
    
    func getProduct(){
        self.db.collection("Categories").document(self.categoryId).collection("Products").document(self.productId)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                let product = Product(
                    document.documentID,
                    document.get("name") as! String,
                    document.get("description") as! String,
                    document.get("code") as! String,
                    document.get("price") as! Double,
                    document.get("imageUrl") as! String
                )
                self.products.append(product)
                self.productsTableView.reloadData()
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredProduct.count : self.products.count
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! ProductTableViewCell
        productId = products[indexPath.row].id
        
        var currentProduct: Product!
        
        //si la bùsqueda està activa saca el pokemon con filter, si no saca todos los pokemones
        if(isFiltering){
            currentProduct = filteredProduct[indexPath.row]
        }else{
            currentProduct = products[indexPath.row]
        }
        
        cell.productNameTextView.text = currentProduct.name
        cell.productImageView.kf.setImage(with: URL(string: currentProduct.imageUrl))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.productId = products[indexPath.row].id
        self.categoryId = categories[indexPath.row]
        performSegue(withIdentifier: "toViewFavouritesProducts", sender: self)
    }
    
    
    func configSearchbar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false //si se le pone true se hace màs oscura la parte de atras de la tabla para saber que estamos en la barra de bùsqueda
        searchController.searchResultsUpdater = self //sabe cual es la tabla que va a controlar
        searchController.searchBar.delegate = self //donde esté escribiendo, donde va a cambiar los resultados.
        searchController.searchBar.sizeToFit() //es para que el searchBar ocupe todo el ancho de la tabla.
        
        productsTableView.tableHeaderView = searchController.searchBar //al table view le pone en el header el search bar.
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredProduct = products.filter{
            ($0.name)
                .lowercased()
                .contains((searchController.searchBar.text ?? "").lowercased()
            )}
        
        isFiltering = searchController.searchBar.text != ""
        productsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toViewFavouritesProducts" {
            let destination = segue.destination as! ClientViewProductViewController
            destination.categoryId = self.categoryId
            destination.productId = self.productId
        }
    }

}
