//
//  CreateAccountViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 6/25/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import Firebase

class CreateAccountViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonCustomization()
    }
    
    func buttonCustomization(){
        self.createAccountButton.layer.cornerRadius = 5
        self.createAccountButton.layer.borderWidth = 1
    }
    
    
    @IBAction func createAccountButtonPressed(_ sender: Any) {
        if(passwordTextField.text == confirmPasswordTextField.text){
           createUserAuth(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
        }else{
            self.showAlertError(tittle: "Error", message: "Passwords do not match")
            emailTextField.text = ""
            passwordTextField.text = ""
            confirmPasswordTextField.text=""
        }
    }

    
    func createUserAuth(email:String, password:String){
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            let user = authResult?.user
            if user != nil {
                //crea referencia a la BD de Firestore
                let db = Firestore.firestore()
                print("USER: \(user!.uid)")
                db.collection("Users").document(user!.uid).setData([
                    "fullName" : "",
                    "birthday": "",
                    "email" : self.emailTextField.text ?? "",
                    "type": "Client"
                    ]
                ){ err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfully written!")
                        self.performSegue(withIdentifier: "toClientNavigation", sender: self)
                    }
                }
            }else{
                self.showAlertError(tittle: "Error", message: "Error in Create Account")
            }
        }
    }
    
    func showAlertError(tittle:String, message:String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self] (_) in
            
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        present(alertView, animated: true, completion: nil)
        alertView.addAction(okAlertAction)
    }
}
