//
//  AddNewCategoryViewController.swift
//  iosProject
//
//  Created by Erika Silva on 7/9/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

class AddNewCategoryViewController: UIViewController,UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var categoryId: String = ""
    var isForViewCategory: Bool = true
    var buttonEdit: UIBarButtonItem?

   
    @IBOutlet weak var categoryImageView: UIImageView!
   
    @IBOutlet weak var categoryName: UITextField!
    
    @IBOutlet weak var categoryDescription: UITextView!
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var addImageButton: UIButton!
    
    
     let imagePickerController = UIImagePickerController()
    
    let db = Firestore.firestore()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(isForViewCategory){
            self.title = "Category Information"
            setToViewInformation()
            buttonEdit = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editButtonTapped))
            self.navigationItem.rightBarButtonItem = buttonEdit
        }
        
        
        categoryImageView.image = #imageLiteral(resourceName: "category")
        
        categoryDescription.delegate = self
        categoryDescription.text = "Description"
        categoryDescription.textColor = UIColor.lightGray
        
        let color = UIColor(red: 65/255, green: 173/255, blue: 170/255, alpha: 1.0).cgColor
        categoryDescription.layer.borderColor = color
        categoryDescription.layer.borderWidth = 2.0
        categoryDescription.layer.cornerRadius = 5
        
        imagePickerController.delegate = self
        
        self.buttonCustomization()
        
    }
    
    func buttonCustomization(){
        self.saveButton.layer.cornerRadius = 5
        self.saveButton.layer.borderWidth = 1
    }
   
    
    
    @objc func cancelButtonTapped(sender: UIBarButtonItem){
        setToViewInformation()
    }
    
    
    func setToViewInformation(){
        addImageButton.isUserInteractionEnabled = false
        categoryImageView.isUserInteractionEnabled = false
        categoryName.isUserInteractionEnabled = false
        categoryName.textColor = UIColor.lightGray
        categoryDescription.isUserInteractionEnabled = false
        categoryDescription.textColor = UIColor.lightGray
        
        buttonEdit = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editButtonTapped))
        self.navigationItem.rightBarButtonItem = buttonEdit
        
        setCategoryInformation()
    }
    
    func setCategoryInformation(){
        let categoryRef = db.collection("Categories").document(categoryId)
        
        categoryRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.categoryName.text = document.get("name") as? String
                self.categoryDescription.text = document.get("description") as? String
                self.categoryImageView.kf.setImage(with: URL(string: document.get("imageUrl") as! String))
                
            } else {
                print("Document does not exist")
            }
        }
        
    }
    
    
    @objc func editButtonTapped(){
        setToEditView()
    }
    
    
    
    
    func setToEditView(){
        addImageButton.isUserInteractionEnabled = true
        categoryImageView.isUserInteractionEnabled = true
        categoryName.isUserInteractionEnabled = true
        categoryName.textColor = UIColor.black
        categoryDescription.isUserInteractionEnabled = true
        categoryDescription.textColor = UIColor.black
        
        self.buttonEdit = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(cancelButtonTapped(sender:)))
        self.navigationItem.rightBarButtonItem = buttonEdit
    }
    
    
    
    
    
    
    @IBAction func addImageButton(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Select Source", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.imagePickerController.sourceType = .camera
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let photoAlbumAction = UIAlertAction(title: "Photo Album", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.imagePickerController.allowsEditing = true
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
       
        alertController.addAction(cameraAction)
        alertController.addAction(photoAlbumAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    //funcion del delegado para poder poner la imagen seleccionada
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else {
            print("Error Picking image")
            return
        }
        categoryImageView.image = image
    }
    
    
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        saveCategoryInfo()
    }
    
    func saveCategoryInfo(){
        if(self.isForViewCategory){
            let categoryRef = db.collection("Categories").document(categoryId)
            categoryRef.updateData([
                "name": categoryName.text ?? "",
                "description": categoryDescription.text ?? "",
                "imageUrl": ""
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    self.storeCategoryPhoto(id: categoryRef.documentID)
                }
            }
        }else{
            var categoryRef: DocumentReference? = nil
            categoryRef = db.collection("Categories").addDocument(data: [
                "name": categoryName.text ?? "",
                "description": categoryDescription.text ?? "",
                "imageUrl": ""
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID")
                    self.storeCategoryPhoto(id: categoryRef!.documentID)
                }
            }
        }
    }
    
    
    func storeCategoryPhoto(id: String){
        let storage = Storage.storage()
        let categoriesImages = storage.reference().child("categories")
        let currentCategoryImage = categoriesImages.child("\(id).jpg")
        
        let categoryImage = categoryImageView.image
        let data = categoryImage?.jpegData(compressionQuality: 1)!
        
        _ = currentCategoryImage.putData(data!, metadata: nil) { (metadata, error) in
            currentCategoryImage.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.updateCategoryImageUrl(id: id, imageUrl: "\(downloadURL)")
            }
        }
    }
    
    
    func updateCategoryImageUrl(id: String, imageUrl: String){
        let categoryRef = db.collection("Categories").document(id)
        
        categoryRef.updateData([
            "imageUrl": imageUrl
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
                self.showAlertError(tittle: "Success", message: "Category was upload Succesfully")
            }
        }
    }
    
    
    func showAlertError(tittle: String, message: String){
        let alertView = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "Ok", style: .default) { [unowned self] (_) in
            _ = self.navigationController?.popViewController(animated: true)
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    
        
    
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if categoryDescription.textColor == UIColor.lightGray {
            categoryDescription.text = ""
            categoryDescription.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if categoryDescription.text == "" {
            categoryDescription.text = "Description"
            categoryDescription.textColor = UIColor.lightGray
        }
    }
    
    
   
    
}
