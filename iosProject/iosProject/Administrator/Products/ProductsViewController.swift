//
//  ProductsViewController.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/19/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import UIKit
import FirebaseFirestore

class ProductsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    var categoryId: String = ""
    var productId: String = ""
    let db = Firestore.firestore()
    var products = [Product]()
    var isForViewProduct: Bool = true
    
    
    var searchController: UISearchController!
    var filteredProduct = [Product]()
    var isFiltering = false
    

    @IBOutlet weak var productsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProducts()
        
        configSearchbar()
    }
    
    
    func getProducts(){
        db.collection("Categories").document(categoryId).collection("Products").addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                print("Error fetching documents: \(error!)")
                return
            }
            self.products = []
            for document in documents {
                let product = Product(
                    document.documentID,
                    document.get("name") as! String,
                    document.get("description") as! String,
                    document.get("code") as! String,
                    document.get("price") as! Double,
                    document.get("imageUrl") as! String
                )
                self.products.append(product)
            }
            self.productsTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredProduct.count : products.count
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! ProductTableViewCell
        productId = products[indexPath.row].id
        
        var currentProduct: Product!
        
        //si la bùsqueda està activa saca el pokemon con filter, si no saca todos los pokemones
        if(isFiltering){
            currentProduct = filteredProduct[indexPath.row]
        }else{
            currentProduct = products[indexPath.row]
        }
        
        cell.productNameTextView.text = currentProduct.name
        cell.productImageView.kf.setImage(with: URL(string: currentProduct.imageUrl))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isForViewProduct = true
        self.productId = products[indexPath.row].id
        performSegue(withIdentifier: "toAddProduct", sender: self)
    }
    
    
    
    
    @IBAction func addProductoButton(_ sender: Any) {
        isForViewProduct = false
        performSegue(withIdentifier: "toAddProduct", sender: self)
    }
    
    
    
    func configSearchbar(){
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false //si se le pone true se hace màs oscura la parte de atras de la tabla para saber que estamos en la barra de bùsqueda
        searchController.searchResultsUpdater = self //sabe cual es la tabla que va a controlar
        searchController.searchBar.delegate = self //donde esté escribiendo, donde va a cambiar los resultados.
        searchController.searchBar.sizeToFit() //es para que el searchBar ocupe todo el ancho de la tabla.
        
        productsTableView.tableHeaderView = searchController.searchBar //al table view le pone en el header el search bar.
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredProduct = products.filter{
            ($0.name)
                .lowercased()
                .contains((searchController.searchBar.text ?? "").lowercased()
            )}
        
        isFiltering = searchController.searchBar.text != ""
        productsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    
    @IBAction func editCategoryButton(_ sender: Any) {
        self.performSegue(withIdentifier: "toEditCategory", sender: self)
    }
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddProduct" {
            let destination = segue.destination as! AddProductViewController
            let viewProduct = self.isForViewProduct
            destination.isForViewproduct = viewProduct
            destination.categoryId = categoryId
            destination.productId = self.productId
        }
        
        if segue.identifier == "toEditCategory" {
            let destination = segue.destination as! AddNewCategoryViewController
            destination.categoryId = categoryId
            destination.isForViewCategory = true
        }
        
    }
    
    
    
    
    

}
