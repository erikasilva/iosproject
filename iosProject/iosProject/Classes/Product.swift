//
//  Product.swift
//  iosProject
//
//  Created by MICHELLE SANCHEZ on 7/19/19.
//  Copyright © 2019 Epn. All rights reserved.
//

import Foundation

class Product{
    var id: String
    var name: String
    var description:String
    var code:String
    var price:Double
    var imageUrl:String
    
    
    init(_ id: String, _ name: String, _ description: String, _ code: String, _ price: Double, _ imageUrl:String) {
        self.id = id
        self.name = name
        self.description = description
        self.code = code
        self.price = price
        self.imageUrl = imageUrl
    }
    
    init(){
        self.id = ""
        self.name = ""
        self.description = ""
        self.code = ""
        self.price = 0.0
        self.imageUrl = ""
    }
}
